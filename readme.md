# 11ty/Pug

### Live example::

-  http://11.intuition.dev

## About

11ty v2.x starter kit w/ Pug templating for sites and web apps. ( Pug https//devhints.io/Pug for SSG templating/html generation) with a 3rd party (Bulma) CSS framework included, under Apache 2.0 license. On Awwwards web site, 11ty is very popular to create great looking sites. Short tutorial on Pug: instead of writing the opening _AND_ closing tags, such as 'body' tag, you just write the tag once; Pug closes it for you. This comes in handy in eliminating bugs for larger web app|site since it gives you errors at edit time). Of course, you can make base 'layouts', 'include partials', etc. I find this more productive than Webflow.

SSG(*) acronym stands for 'generated' / JAMstack) content is generated at the 'edit' stage, ahead of use. Compared to SSR(ex Wordpress|Laravel) that 'generates/renders' at runtime: when the user views page|content. SSG needs all the data to be there ahead of use (or else it has to use fetch|API or REST calls for any dynamic data), for example a JSON file list of items, such as blog posts, employees, etc. SSG is better at SEO, and therefore it is better at content marketing relative to SSR. However, you will have to at least glance the 'Pug' doc linked above - so there is something that you have to learn to use this starter kit. Think of Pug as a more powerful version of Markdown. Markdown only does some html tags and very little CSS: Pug does *ANYTHING\* html can.

In src/assets folder you'll see some sample fonts you could chose to use, and some js libs that you could chose to use. It uses Bulma CSS Library ( https://github.com/aldi/awesome-bulma-templates )
## Install

-  npm i
-  npm run dev
-  when it runs, it will also give you a debug port, good for mobile development

## Notes:

-  It has a very nice type-to-search on blogs page.
-  There are online 'html 2 pug' helpers, ex http://html2pug.vercel.app
-  This is 11ty v2.x. For 11ty v1.x example: http://gitlab.com/cekvenich2/ubay
-  Another example of 11ty v1.x, by Kevin Powell http://youtube.com/watch?v=4wD00RT6d-g has simple admin panel (with code http://codementor.io/project-solutions/c96tvkjhxi )
-  If you don't like this, there are other SSG here http://JAMstack.org/generators, or MiddleMan w/ Slim: http://gitlab.com/cekvenich2/middlemanapp-slim-template
-  As convention, create a folder for a page so that you can call the file index.pug
-  Includes responsive/Aria compliant 3rd party NavBar

## Roadmap

-  Add sitemap plugin
-  Good Alpine example

## More tools I use. If you have a cool tool you use, please open a ticket and let me know to try it. Please!

-  https://labinator.com/wordpress-marketplace/labu-demo
-  Image Size http://squoosh.app
-  http://realfavicongenerator.net
-  Icons http://feathericons.com
-  http://balsamiq.com/wireframes
-  http://whimsical.com/wireframes
-  http://cloudconvert.com/ttf-to-woff2
-  http://gtmetrix.com/analyze.html

## Other:

-  Source of images:
-  http://vecteezy.com/photo/7187237-overjoyed-young-beautiful-woman-standing-by-bedroom-and-opening-curtains-look-in-window-distance-meet-welcome-new-day-smiling-female-feel-excited-about-life-career-opportunities-or-perspectives
-  http://pexels.com/photo/view-of-street-from-a-glass-window-531880
-  Another admin panel: - http://cloudcannon.com/eleventy-cms
-  A good YT channel, here is an example episode: http://youtube.com/watch?v=lgz4z9uvwgw

## Example HTTP Server config:
```
sub.domain.tld {
	encode gzip
	root * /home/i-host/11pug/public
	file_server
	@static {
		file
		path *.ico *.min.css *.min.js *.gif *.jpg *.jpeg *.png *.webp *.woff2 *.pdf
		}

	header @static Cache-Control max-age=86400
}
```

### Much thanks to @Snapstromegon( http://hoeser.dev ) for guidance,
